A simple but complete solution for backups in git over (networked) filesystems.

Hosting the git repository on a distant filesystem is slower, but avoids
duplicating the backup on the machine itself. The program is configurable to
backup on the same machine (you would then push to have a backup in an other
location).

It also includes history rewriting to get rid of too frequent commits, and keep
the repository size in check.